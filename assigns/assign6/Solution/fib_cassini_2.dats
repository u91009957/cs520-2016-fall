(*
extern
prfun
fib_cassini
{n:nat}
{f0,f1,f2:int}
{i:int}
(
  pf0: FIB (n, f0)
, pf1: FIB (n+1, f1)
, pf2: FIB (n+2, f2)
, pf3: SGN (n, i)
) : [f0*f2 + i == f1*f1] void
*)

stacst sgn : int -> int
stacst fib : int -> int
//
// Cassini's formula states:
// fib(n)*fib(n+2) + (-1)^n = (fib(n+1))^2
//
(*
  | FIBbas0 (0, 0) of ()
  | FIBbas1 (1, 1) of ()
  | {n:nat}{r0,r1:int}
    FIBind2 (n+2, r0+r1) of (FIB (n, r0), FIB (n+1, r1))
*)

extern
praxi
lemma_sgn_0(): [sgn(0) == 1] void
extern
praxi
lemma_sgn_1{n:pos} (): [sgn(n) == ~sgn(n-1)] void
//
extern
praxi
lemma_fib_0(): [fib(0) == 0] void
extern
praxi
lemma_fib_1(): [fib(1) == 1] void
extern
praxi
lemma_fib_2{n:int | n >= 2} (): [fib(n) == fib(n-2) + fib(n-1)] void

prfun
fib_cassini_2{n:nat} .<n>.
(
) : [fib(n)*fib(n+2) + sgn(n) == fib(n+1)*fib(n+1)] void =
(
sif
(n==0)
then () where
{
  prval () = lemma_sgn_0()
  prval () = lemma_fib_0()
  prval () = lemma_fib_1()
  prval () = lemma_fib_2{2}()
}
else () where
{
  prval () = lemma_sgn_1{n}()
  prval () = lemma_fib_2{n+1}()
  prval () = lemma_fib_2{n+2}()
  prval () = fib_cassini_2{n-1}()
}
)