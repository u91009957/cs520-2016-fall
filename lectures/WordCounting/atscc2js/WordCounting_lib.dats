(*
** Enumerate words
** according to their frequencies
** in a given source
*)

(* ****** ****** *)
//
#ifdef
WordCounting
#then
#else
#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"
#endif // ifdef(WordCounting)
//
(* ****** ****** *)
//
#staload "./WordCounting.sats"
//
(* ****** ****** *)
//
#include
"./../SHARE/WordCounting_lib.dats"
//
(* ****** ****** *)

(* end of [WordCounting_lib.dats] *)
