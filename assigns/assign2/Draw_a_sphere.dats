(* ****** ****** *)

(*
//
// Here is a program for drawing sphere
// in ascii characters
//
// http://rosettacode.org/wiki/Draw_a_sphere#C
//
*)

(* ****** ****** *)

staload "libats/libc/SATS/math.sats"
staload "libats/libc/SATS/stdio.sats"

(* ****** ****** *)
//
// HX: 15 points
//
(* ****** ****** *)
//
extern
fun
DrawSphere
(
  R: double, k: double, ambient: double
) : void // end of [Draw_a_sphere]
//
(* ****** ****** *)

(* end of [Draw_a_sphere.dats] *)
