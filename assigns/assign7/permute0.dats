(*
** Listing all the
** permutations of a given list
*)

(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
stadef
tup(a:t@ype, b:t@ype) = $tup(a, b)
stadef
tup(a:t@ype, b:t@ype, c:t@ype) = $tup(a, b, c)
//
(* ****** ****** *)
//
extern
fun
{a:t0ype}
permute0
  (xs: list0(INV(a))): list0(list0(a))
//
(* ****** ****** *)
//
extern
fun
{a:t0p}
choose1
(
xs: list0(INV(a))
) : list0(tup(a, list0(a)))
//
implement
{a}(*tmp*)
choose1(xs) =
(
case+ xs of
| nil0() =>
  list0_nil()
| cons0(x0, xs) =>
  list0_cons(
    $tup(x0, xs)
  , (choose1(xs)).map(TYPE{tup(a, list0(a))})(lam($tup(x1, xs)) => $tup(x1, cons0(x0, xs)))
  )(* list0_cons *)
)
//
(* ****** ****** *)
//
implement
{a}(*tmp*)
permute0
  (xs) =
(
case+ xs of
| list0_nil() =>
  list0_sing(list0_nil)
| list0_cons _ =>
  list0_concat((choose1(xs)).map(TYPE{list0(list0(a))})(lam($tup(x, xs)) => list0_mapcons(x, permute0(xs))))
)
//
(* ****** ****** *)

implement
main0() =
{
//
val xs =
$list{int}(0,1,2,3,4)
//
val xs = g0ofg1(xs)
//
val xss = permute0(xs)
//
val ((*void*)) =
fprint_listlist0_sep(stdout_ref, xss, "\n", "|")
//
val ((*void*)) = fprintln!(stdout_ref)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [permute0.dats] *)
