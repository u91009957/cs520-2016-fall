(*
** Solution to assign2.dats
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats" // defines some names
#include
"share/atspre_staload.hats" // for targeting C
#include
"share/HATS/atspre_staload_libats_ML.hats" // for ...
//
(* ****** ****** *)

#include "./../assign2.dats"

(* ****** ****** *)
//
// Please do your implementation below:
//
(* ****** ****** *)



(* ****** ****** *)

implement
main0() =
{
//
val () =
println! ("int_test() = ", int_test())
//
val () = assertloc (ghaap(0) = gheep(0))
val () = assertloc (ghaap(1) = gheep(1))
val () = assertloc (ghaap(5) = gheep(5))
val () = assertloc (ghaap(10) = gheep(10))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [assign2_sol.dats] *)
