(* ****** ****** *)
//
#include
"./../ConcurrentSideBisectors.dats"
//
(* ****** ****** *)
//
// Please write your code below:
//
(* ****** ****** *)

stadef
A_bisect(x: real, y: real) =
(x-(x2+x3)/2)*(x2-x3)+(y-(y2+y3)/2)*(y2-y3)

stadef
B_bisect(x: real, y: real) =
(x-(x3+x1)/2)*(x3-x1)+(y-(y3+y1)/2)*(y3-y1)

stadef
C_bisect(x: real, y: real) =
(x-(x1+x2)/2)*(x1-x2)+(y-(y1+y2)/2)*(y1-y2)

(* ****** ****** *)
//
extern
prfun
MainLemma
{ x,y:real
| A_bisect(x, y)==0 && B_bisect(x, y)==0
} ((*void*)): [C_bisect(x, y) == 0] void
//
(* ****** ****** *)
//
primplmnt MainLemma{x,y}() = ()
//
(* ****** ****** *)

(* end of [ConcurrentSideBisectors_sol.dats] *)
