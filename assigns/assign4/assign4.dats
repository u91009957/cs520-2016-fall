(*
** Assignment 4
** It is due Wednesday, the 5th of October, 2016
*)

(* ****** ****** *)

(*
** A word consists of a non-empty sequence of letters.
** More precisely, the regular expression for word as
** [A-Za-z]+
*)

(* ****** ****** *)
//
typedef
word = string
//
(* ****** ****** *)
//
// HX: 20 points
//
extern
fun
stream_wordizing
  (stream(char)): stream(word)
//
(* ****** ****** *)
//
// HX: 10 bonus points
//
extern
fun
stream_vt_wordizing
  (stream_vt(char)): stream_vt(word)
//
(* ****** ****** *)
//
extern
fun
assign4_test(): void
//
implement
assign4_test() = let
//
val ws =
stream_vt_wordizing
  (streamize_fileref_char(stdin_ref))
//
val ws = stream_vt2t(ws)
//
fun
loop(ws: stream(word)): void =
  case+ !ws of
  | stream_nil() => ()
  | stream_cons(w, ws) => (println!(w); loop(ws))
//
in
  loop(ws)
end // end of [assign4_test]
//
(* ****** ****** *)

(* end of [assign4.dats] *)
