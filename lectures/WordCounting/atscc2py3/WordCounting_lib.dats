(*
** Enumerate words
** according to their frequencies
** in a given source
*)

(* ****** ****** *)
//
#ifdef
WordCounting
#then
#else
#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2PY3}/staloadall.hats"
#endif // ifdef(WordCounting)
//
(* ****** ****** *)
//
#staload "./WordCounting.sats"
//
(* ****** ****** *)
//
#include
"./../SHARE/WordCounting_lib.dats"
//
(* ****** ****** *)
//
macdef
list0_sing(x) =
  list0_cons(,(x), list0_nil)
//
(* ****** ****** *)
//
extern
fun
ord: strchr -> int = "mac#"
extern
fun
chr: int -> strchr = "mac#"
//
(* ****** ****** *)

val _a_ = ord("a")
val _z_ = ord("z")
val _A_ = ord("A")
val _Z_ = ord("Z")

(* ****** ****** *)
//
fun
isalpha(c: int): bool =
   (c >= _a_ && c <= _z_)
|| (c >= _A_ && c <= _Z_)
//
(* ****** ****** *)
//
fun
tolower(c: int): int =
(
if (c >= _A_ && c <= _Z_) then c + 32 else c
)
//
(* ****** ****** *)
//
implement
word_get() = let
//
fun
skip(): int = let
  val c = char_get()
(*
//
val () =
  println! ("skip: c = ", c)
//
*)
in
//
if
c >= 0
then
(
 if isalpha(c) then c else skip()
) else ~1 // end of [else]
//
end
//
fun
get2
(
cs: list0(int)
) : list0(int) = let
  val c = char_get()
in
//
if
isalpha(c)
then get2(list0_cons(tolower(c), cs)) else cs
// end of [if]
//
end // end of [get2]
//
val c0 = skip()
//
in
//
if
c0 >= 0
then let
//
val cs = list0_sing(tolower(c0))
val cs = PYlist_oflist_rev(g1ofg0(get2(cs)))
//
in
  PYlist_string_join(PYlist_map(cs, lam(c) => chr(c)))
end (* end of [then] *)
else "" // end of [else]
//
end // end of [word_get]

(* ****** ****** *)
//
implement
mylist_sort_string
  (xs) = list0_sort_1(xs)
//
(* ****** ****** *)
//
implement
mylist_sort_intstr(nxs) = let
//
fun
mycmp
(
  nx1: intstr
, nx2: intstr
) : int = let
//
val sgn = ~compare(nx1.0, nx2.0)
//
in
  if sgn != 0 then sgn else compare(nx1.1, nx2.1)
end // end of [gcompare_val_val]
//
in
  list0_sort_2(nxs, lam(nx1, nx2) => mycmp(nx1, nx2))
end // end of [mylist_sort_intstr]
//
(* ****** ****** *)

(* end of [WordCounting_lib.dats] *)
